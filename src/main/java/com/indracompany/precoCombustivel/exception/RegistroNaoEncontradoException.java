package com.indracompany.precoCombustivel.exception;

@SuppressWarnings("serial")
public class RegistroNaoEncontradoException extends Exception {
	
	public RegistroNaoEncontradoException(){
		super("Registro não encontrado");
	}
	
	public RegistroNaoEncontradoException(String msg){
		super(msg);
	}

}
