package com.indracompany.precoCombustivel.exception;


@SuppressWarnings("serial")
public class UserAlreadyExistsException extends Exception {
	
	public UserAlreadyExistsException(String msg) {
		super(msg);
	}

}
