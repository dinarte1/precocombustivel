package com.indracompany.precoCombustivel.exception;

@SuppressWarnings("serial")
public class FileNotFormatedExcption extends RuntimeException{

	public FileNotFormatedExcption(String msg) {
		super(msg);
	}
	
}
