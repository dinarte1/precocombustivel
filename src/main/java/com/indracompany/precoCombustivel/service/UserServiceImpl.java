package com.indracompany.precoCombustivel.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import com.indracompany.precoCombustivel.domain.User;
import com.indracompany.precoCombustivel.exception.RegistroNaoEncontradoException;
import com.indracompany.precoCombustivel.exception.UserAlreadyExistsException;
import com.indracompany.precoCombustivel.repository.UserRepository;

@Service @RequestScope @Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepository;
	
	
	@Override
	public void save(User user) throws UserAlreadyExistsException {
		if (userRepository.existsByUsername(user.getUsername()))
			throw new UserAlreadyExistsException("Já existe um usuário com o mesmo nome em nosso sistema");
		userRepository.save(user);
	}

	@Override
	public List<User> list() {
		return (List<User>) userRepository.findAll();
	}

	@Override
	public void deleteOne(Long id) throws RegistroNaoEncontradoException {
		if (!userRepository.existsById(id))
			throw new RegistroNaoEncontradoException("Não encontramos o User #"+id);
		
	}
	

}
