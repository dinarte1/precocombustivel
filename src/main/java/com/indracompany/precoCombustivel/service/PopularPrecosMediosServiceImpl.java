package com.indracompany.precoCombustivel.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.indracompany.precoCombustivel.repository.PrecoMedioPorBandeiraRepository;
import com.indracompany.precoCombustivel.repository.PrecoMedioPorMunicipioRepository;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class PopularPrecosMediosServiceImpl implements PopularPrecosMediosService {

	@Autowired
	PrecoMedioPorMunicipioRepository porMunicipioRepoitory;
	
	@Autowired
	PrecoMedioPorBandeiraRepository porBandeiraRepository;
	
	/* (non-Javadoc)
	 * @see com.indracompany.precoCombustivel.service.PopularPrecosMediosService#popularPrecosMedios()
	 */
	@Override
	@Transactional
	public void popularPrecosMedios() {
		log.info("Calculando de preços médios por municipios, isso vai durar 1 minuto...");
		porMunicipioRepoitory.populePrecosMediosByMunicipio();
		log.info("Calculando de preços médios por bandeira, isso vai durar 1 minuto...");
		porBandeiraRepository.populePrecosMediosByBandeira();
		log.info("Fim da população de preços médios");
	}
	
}
