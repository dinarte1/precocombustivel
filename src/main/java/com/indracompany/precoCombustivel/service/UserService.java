package com.indracompany.precoCombustivel.service;

import java.util.List;

import com.indracompany.precoCombustivel.domain.User;
import com.indracompany.precoCombustivel.exception.RegistroNaoEncontradoException;
import com.indracompany.precoCombustivel.exception.UserAlreadyExistsException;

public interface UserService {

	void save(User precoCombustivel) throws UserAlreadyExistsException;
	
	public List<User> list();
	
	public void deleteOne(Long id) throws RegistroNaoEncontradoException;
	
}