package com.indracompany.precoCombustivel.service;

import java.io.BufferedReader;
import java.util.List;

import org.springframework.data.domain.Pageable;

import com.indracompany.precoCombustivel.domain.PrecoCombustivel;
import com.indracompany.precoCombustivel.exception.FileNotFormatedExcption;
import com.indracompany.precoCombustivel.exception.RegistroNaoEncontradoException;

public interface PrecoCombustivelSevice {

	void save(PrecoCombustivel precoCombustivel);
	
	public List<PrecoCombustivel> list(Pageable page);
	
	public void deleteOne(Long id) throws RegistroNaoEncontradoException;


	/**
	 * Recebe um BufferedReader da leitura do arquivo csv e faz sua leitura fazendo uso de processamento paralelo
	 * varendo as linhas, transformando em objeto e persistindo no banco.
	 * @param reader
	 * @throws FileNotFormatedExcption 
	 */
	public void migrarUsandoProcessamentoParalelo(BufferedReader reader) throws FileNotFormatedExcption;
	
	

	

}