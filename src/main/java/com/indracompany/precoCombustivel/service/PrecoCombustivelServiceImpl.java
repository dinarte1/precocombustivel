package com.indracompany.precoCombustivel.service;

import java.io.BufferedReader;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import com.indracompany.precoCombustivel.domain.PrecoCombustivel;
import com.indracompany.precoCombustivel.exception.FileNotFormatedExcption;
import com.indracompany.precoCombustivel.exception.RegistroNaoEncontradoException;
import com.indracompany.precoCombustivel.repository.PrecoCombustivelRepository;
import com.indracompany.precoCombustivel.utils.ParsingUtils;

import lombok.extern.log4j.Log4j2;

@Service @RequestScope @Log4j2
public class PrecoCombustivelServiceImpl implements PrecoCombustivelSevice {
	
	@Autowired
	private PrecoCombustivelRepository precoCombustivelRepository;
	
	@Autowired
	private ParsingUtils parsingUtils;
	
	@Override
	@Transactional
	public void migrarUsandoProcessamentoParalelo(BufferedReader reader) throws FileNotFormatedExcption {
	
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");	
		reader
			.lines()
			.parallel()
			.forEach(line ->{
				if (!line.contains("Data da Coleta")) { //Skipping first line
					try {
						String[] valores = line.split("	");
						
						PrecoCombustivel precoCombustivel = new PrecoCombustivel(null, valores[0], valores[1], valores[2], valores[3], valores[4], 
								LocalDate.parse(valores[5], formatter), parsingUtils.stringToBigDecimal( valores[6] ), parsingUtils.stringToBigDecimal( valores[7] ), 
								valores[8], valores[9]);
						save(precoCombustivel);
						log.info("SUCESS " + line);
					} catch (ParseException e) {
						log.error("ERROR  " + line );
						log.error(e.getMessage());
					} catch (ArrayIndexOutOfBoundsException e) {
						throw new FileNotFormatedExcption("Parece que o arquivo que você enviou não está no formato esperado.");
					}
				}	
			});
	}

	
	
	@Override
	public void save(PrecoCombustivel precoCombustivel) {
		precoCombustivelRepository.save(precoCombustivel);
	}

	@Override
	public List<PrecoCombustivel> list(Pageable page) {
		return (List<PrecoCombustivel>) precoCombustivelRepository.findAllOrdeenado(page);
	}

	@Override
	public void deleteOne(Long id) throws RegistroNaoEncontradoException {
		if (!precoCombustivelRepository.existsById(id))
			throw new RegistroNaoEncontradoException("Não encontramos o registro de preco #"+id);
		precoCombustivelRepository.deleteById(id);
	}

	
	

}
