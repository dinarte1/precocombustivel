package com.indracompany.precoCombustivel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import com.indracompany.precoCombustivel.domain.User;
import com.indracompany.precoCombustivel.exception.RegistroNaoEncontradoException;
import com.indracompany.precoCombustivel.exception.UserAlreadyExistsException;
import com.indracompany.precoCombustivel.service.UserService;

import io.swagger.annotations.Api;

@Api("Recusro de manutenção dp histórico de preços")
@RestController
@RequestScope
public class GerenciadorDeUsuariosController {

	@Autowired
	UserService userService;
	
	@GetMapping("/user")
	public List<User> list(){
		return (List<User>) userService.list();
	}
	
	@PostMapping("/user")
	public User save(@RequestBody User user) throws UserAlreadyExistsException {
		userService.save(user);
		return user;
	}
	
	@PutMapping("/user")
	public User update(@RequestBody User user) throws UserAlreadyExistsException {
		userService.save(user);
		return user;
	}
	
	@DeleteMapping("/user/{id}")
	public void deleteOne(@PathVariable("id") Long id) throws RegistroNaoEncontradoException {
		userService.deleteOne(id);
	}
	
}
