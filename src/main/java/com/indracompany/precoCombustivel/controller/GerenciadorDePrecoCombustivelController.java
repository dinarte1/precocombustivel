package com.indracompany.precoCombustivel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import com.indracompany.precoCombustivel.domain.PrecoCombustivel;
import com.indracompany.precoCombustivel.exception.RegistroNaoEncontradoException;
import com.indracompany.precoCombustivel.service.PrecoCombustivelSevice;

@RestController
@RequestScope
public class GerenciadorDePrecoCombustivelController {

	@Autowired
	PrecoCombustivelSevice precoCombustivelService;
	
	@GetMapping("/precoCombustivel")
	public List<PrecoCombustivel> list(Pageable page){
		return (List<PrecoCombustivel>) precoCombustivelService.list(page);
	}
	
	@PostMapping("/precoCombustivel")
	public PrecoCombustivel save(@RequestBody PrecoCombustivel precoCombustivel) {
		precoCombustivelService.save(precoCombustivel);
		return precoCombustivel;
	}
	
	@PutMapping("/precoCombustivel")
	public PrecoCombustivel update(@RequestBody PrecoCombustivel precoCombustivel) {
		precoCombustivelService.save(precoCombustivel);
		return precoCombustivel;
	}
	
	@DeleteMapping("/precoCombustivel/{id}")
	public void deleteOne(@PathVariable("id") Long id) throws RegistroNaoEncontradoException {
		precoCombustivelService.deleteOne(id);
	}
	
}
