package com.indracompany.precoCombustivel.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.indracompany.precoCombustivel.domain.PrecoCombustivel;
import com.indracompany.precoCombustivel.domain.PrecoMedioPorBandeira;
import com.indracompany.precoCombustivel.domain.PrecoMedioPorMunicipio;
import com.indracompany.precoCombustivel.repository.PrecoCombustivelRepository;
import com.indracompany.precoCombustivel.repository.PrecoMedioPorBandeiraRepository;
import com.indracompany.precoCombustivel.repository.PrecoMedioPorMunicipioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("Consulta de dados do histórico de preço de combustíveis")
@RestController
public class ConsultaPrecosController {
	
	@Autowired
	PrecoCombustivelRepository precoCombusivelRepository;
	
	@Autowired
	PrecoMedioPorMunicipioRepository precoMedioPorMunicipioRepository;
	
	@Autowired
	PrecoMedioPorBandeiraRepository precoMedioPorBandeiraReository;
	
	@ApiOperation(value = "Retorna todo o histórico de uma dada região de forma paginada, 20 em 20 registros.")
	@GetMapping("/precos/regiao/{regiao}")
	public List<PrecoCombustivel> precosPrRegiao(@PathVariable("regiao") String regiao, Pageable pageable){
		return precoCombusivelRepository.findAllByRegiao(regiao, pageable);
	}
	
	@ApiOperation(value = "Retorna o preço medio de um dado município")
	@GetMapping("/precos/media/municipio/{municipio}")
	public BigDecimal precoMedioDoMunicipio(@PathVariable("municipio") String municipio){
		return precoCombusivelRepository.mediaPrecoPorMunicipio(municipio);
	}
	
	@ApiOperation(value = "Retorna as preços médios de compar a e venda do combustível de cada município")
	@GetMapping("/precos/mediaCompraEVenda/municipio/")
	public List<PrecoMedioPorMunicipio> precoMedioCompaEVendaPorAgrupadoPorMunicipo(){
		return precoMedioPorMunicipioRepository.findAllOrderByMunicipio();
	}

	@ApiOperation(value = "Retorna as preços médios de compar a e venda do combustível de cada fornecedor (Bandeira)")
	@GetMapping("/precos/mediaCompraEVenda/bandeira/")
	public List<PrecoMedioPorBandeira> precoMedioCompaEVendaPorAgrupadoPorBandeira(){
		return precoMedioPorBandeiraReository.findAllOrderByBandeira();
	}
}
