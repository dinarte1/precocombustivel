package com.indracompany.precoCombustivel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * Controlador com a responsabilidade unica de iniciar o app redirecionando para o front-end
 * @author dinarte
 */

import com.indracompany.precoCombustivel.service.UserService;
@Controller
public class HomeController {
	
	UserService userService;
	
	@GetMapping("/")
	public String home() {	
		return "redirect:index.html";
	}
	
	
}
