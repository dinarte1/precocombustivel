package com.indracompany.precoCombustivel.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.indracompany.precoCombustivel.exception.FileNotFormatedExcption;
import com.indracompany.precoCombustivel.service.PopularPrecosMediosService;
import com.indracompany.precoCombustivel.service.PrecoCombustivelSevice;
import com.indracompany.precoCombustivel.vo.PostMigrationVo;

/**
 * Controllador com a responsabilidade única de receber o arquivo a ser migrado
 * transformando-o em uma lista de objetos do tipo
 * <code>PrecoCombustivel.java</code>
 * 
 * @author dinarte
 */
@RestController
public class ImportarDoCsvController {

	@Autowired
	private PrecoCombustivelSevice precoCombustivelService;
	
	@Autowired
	private PopularPrecosMediosService popularPrecosMediosService;
	
	/**
	 * Recurso responsável por receber o arquivo e enviá-lo para o service.
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws FileNotFormatedExcption 
	 */
	@PostMapping("/importarDoCsv")
	public PostMigrationVo importarDoCsv(@RequestParam("file") MultipartFile file) throws IOException, FileNotFormatedExcption {
		
		if (!file.getOriginalFilename().contains(".csv"))
			throw new FileNotFormatedExcption("O aqruivo eviado deve possuir a extensão .csv");
		BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()));
		precoCombustivelService.migrarUsandoProcessamentoParalelo(reader);
		popularPrecosMediosService.popularPrecosMedios();
		return new PostMigrationVo(file.getOriginalFilename(),file.getContentType(),file.getSize());
	}
	
}
