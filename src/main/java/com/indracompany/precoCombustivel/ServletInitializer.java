package com.indracompany.precoCombustivel;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Classe responsável por habilitar o uso do projeto dentro de um container Tomcat
 * @author dinarte
 *
 */
public class ServletInitializer extends SpringBootServletInitializer{
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		
		return application.sources(PrecoCombustivelApplication.class);
	}

}
