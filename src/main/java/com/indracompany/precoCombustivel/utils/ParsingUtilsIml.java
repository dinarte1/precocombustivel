package com.indracompany.precoCombustivel.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

import org.springframework.stereotype.Component;

/**
 * Componente Util para transformações de dados.
 * @author dinarte
 */
@Component
public class ParsingUtilsIml implements ParsingUtils {
	
	/**
	 * Método capaz de transformar uma string no padrão 23.989,00 em um objeto BigDecimal
	 */
	public BigDecimal stringToBigDecimal(String valor) throws ParseException {
		
		if (valor.trim().equals("")) 
			return null;
		
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		symbols.setDecimalSeparator(',');
		String pattern = "#,##0.0#";
		DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
		decimalFormat.setParseBigDecimal(true);

		// parse the string
		BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse(valor);
		return bigDecimal;
	}

}
