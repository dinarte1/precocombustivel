package com.indracompany.precoCombustivel.utils;

import java.math.BigDecimal;
import java.text.ParseException;

public interface ParsingUtils {
	
	
	public BigDecimal stringToBigDecimal(String valor) throws ParseException;

}