package com.indracompany.precoCombustivel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final Contact DEFAULT_CONTACT = new Contact("Dinarte Alves", "https://www.linkedin.com/in/dinarte-filho-8bb8496/", "dinarte@gmail.com");
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("Api - Preço do Combustível", "Documentação Interativa", "1.0", "urn:tos",DEFAULT_CONTACT, "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
	private static final Set<String> DEFAULT_PRODUCES_AND_COSUMES = new HashSet<String>(Arrays.asList("application/json"));
	
	@Bean
	public Docket api() {
		
		return new Docket(DocumentationType.SWAGGER_2)
                .select()           
                .apis(RequestHandlerSelectors.basePackage("com.indracompany.precoCombustivel.controller"))
                .build()
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_PRODUCES_AND_COSUMES);			
	}
	
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
 
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
	
	
}
