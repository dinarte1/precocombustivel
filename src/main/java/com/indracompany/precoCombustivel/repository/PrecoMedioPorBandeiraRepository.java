package com.indracompany.precoCombustivel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.indracompany.precoCombustivel.domain.PrecoMedioPorBandeira;

@Repository
public interface PrecoMedioPorBandeiraRepository extends CrudRepository<PrecoMedioPorBandeira, Long> {
	
	@Query("from PrecoMedioPorBandeira order by bandeira")
	public List<PrecoMedioPorBandeira> findAllOrderByBandeira();
	
	/**
	 * Popula os preços médios de compra e de venda do combustivel por bandeira
	 * @return
	 */
	@Modifying
	@Query(value="INSERT INTO PRECO_MEDIO_POR_BANDEIRA (ID, BANDEIRA,  MEDIA_COMPRA, MEDIA_VENDA ) "
			+ "SELECT NEXTVAL('HIBERNATE_SEQUENCE'), bandeira, SUM(mediaCompra) AS madiaCompra, SUM(mediaVenda) AS mediaVenda \n" + 
			"FROM (\n" + 
			"SELECT BANDEIRA, sum(valor_compra) / COUNT(*)  AS mediaCompra, 0 as mediaVenda\n" + 
			"FROM PRECO_COMBUSTIVEL \n" + 
			"WHERE valor_compra IS NOT NULL\n" + 
			"GROUP BY BANDEIRA\n" + 
			"\n" + 
			"UNION ALL\n" + 
			"\n" + 
			"SELECT BANDEIRA, 0 as mediaCompra,  sum(valor_venda) / count(*) as mediaVenda\n" + 
			"FROM PRECO_COMBUSTIVEL \n" + 
			"WHERE valor_venda IS NOT NULL\n" + 
			"GROUP BY BANDEIRA\n" + 
			") AS FOOL\n" + 
			"GROUP BY BANDEIRA\n" + 
			"ORDER BY BANDEIRA", nativeQuery = true)
	public void populePrecosMediosByBandeira();

}
