package com.indracompany.precoCombustivel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.indracompany.precoCombustivel.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	public User findByUsername(String username);
	
	public Boolean existsByUsername(String username);

}
