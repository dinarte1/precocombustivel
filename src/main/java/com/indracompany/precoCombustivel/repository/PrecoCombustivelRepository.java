package com.indracompany.precoCombustivel.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.indracompany.precoCombustivel.domain.PrecoCombustivel;


@Repository
public interface PrecoCombustivelRepository extends CrudRepository<PrecoCombustivel, Long> {
	
	/**
	 * Executa uma busca paginada dos precos por regiao
	 * @param regiao
	 * @param pageable
	 * @return
	 */
	public List<PrecoCombustivel> findAllByRegiao(@Param("regiao") String regiao, Pageable pageable);
	
	/**
	 * Retorna o valor médio de venda do combustivel dado um municipio.
	 * @param municipio
	 * @return
	 */
	@Query("select sum(valorVenda) / count(id) from PrecoCombustivel where municipio = :municipio")
	public BigDecimal mediaPrecoPorMunicipio(@Param("municipio") String municipio);

	/**
	 * Retorna a listagem completa paginada ordenada 
	 * @param page
	 * @return
	 */
	@Query("from PrecoCombustivel order by regiao, estado, municipio, bandeira")
	public List<PrecoCombustivel> findAllOrdeenado(Pageable page);
	
	
	
}
