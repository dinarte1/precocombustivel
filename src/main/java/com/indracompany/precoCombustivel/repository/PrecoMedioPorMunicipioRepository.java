package com.indracompany.precoCombustivel.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.indracompany.precoCombustivel.domain.PrecoMedioPorMunicipio;

@Repository
public interface PrecoMedioPorMunicipioRepository extends CrudRepository<PrecoMedioPorMunicipio, Long> {
	
	@Query("from PrecoMedioPorMunicipio order by municipio")
	public List<PrecoMedioPorMunicipio> findAllOrderByMunicipio();
	
	/**
	 * Popula os preços médios de compra e de venda do combustivel por municipio.
	 */
	@Modifying
	@Query(value="INSERT INTO PRECO_MEDIO_POR_MUNICIPIO (ID, MUNICIPIO,  MEDIA_COMPRA, MEDIA_VENDA ) "
			+ "SELECT NEXTVAL('HIBERNATE_SEQUENCE'), municipio, SUM(mediaCompra) AS madiaCompra, SUM(mediaVenda) AS mediaVenda \n" + 
			"FROM (\n" + 
			"SELECT MUNICIPIO, sum(valor_compra) / COUNT(*)  AS mediaCompra, 0 as mediaVenda\n" + 
			"FROM PRECO_COMBUSTIVEL \n" + 
			"WHERE valor_compra IS NOT NULL\n" + 
			"GROUP BY MUNICIPIO\n" + 
			"\n" + 
			"UNION ALL\n" + 
			"\n" + 
			"SELECT MUNICIPIO, 0 as mediaCompra,  sum(valor_venda) / count(*) as mediaVenda\n" + 
			"FROM PRECO_COMBUSTIVEL \n" + 
			"WHERE valor_venda IS NOT NULL\n" + 
			"GROUP BY MUNICIPIO\n" + 
			") AS FOOL\n" + 
			"GROUP BY MUNICIPIO\n" + 
			"ORDER BY MUNICIPIO", nativeQuery = true)
	public void populePrecosMediosByMunicipio();
	

}
