package com.indracompany.precoCombustivel.security;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * Configura os filters e cria o bean de condfigutration 
 * @author dinarte
 *
 */
@EnableWebSecurity
public class SpringJWTIntegrationConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JWTConfig jwtConfig;

	@Autowired
	private UserDetailsService userDetailsService;
	
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	
		http.csrf().disable()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.exceptionHandling()
				.authenticationEntryPoint((req, rsp, e) -> rsp.sendError(HttpServletResponse.SC_UNAUTHORIZED)).and()
				//This filter will process new logins
				.addFilter(new JWTLoginFilter(authenticationManager(), jwtConfig))
				.authorizeRequests()
				//This will configure free access to the authentication URL
				.antMatchers(HttpMethod.POST, jwtConfig.getUri()).permitAll()
				.antMatchers(HttpMethod.GET, "/").permitAll()
				.antMatchers(HttpMethod.GET, "/index.html").permitAll()
				.and()
				.authorizeRequests()
				.antMatchers("/app/**", "/importarDoCsv","/h2/**","/v2/api-docs**", "/configuration/ui/**", "/swagger-resources/**","swagger-resources/configuration/**", "/configuration/security/**", "/swagger-ui.html**", "/webjars/**")			
				.permitAll()
				.and()
				.addFilterAfter(new JWTVerifyAuthenticationFilter(jwtConfig), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests().anyRequest().authenticated();
		
		http.headers().frameOptions().disable();
		
	}
	


	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public JWTConfig jwtConfig() {
		return new JWTConfig();
	}
}
