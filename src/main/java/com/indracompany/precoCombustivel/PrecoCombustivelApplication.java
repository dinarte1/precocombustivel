package com.indracompany.precoCombustivel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrecoCombustivelApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrecoCombustivelApplication.class, args);
	}

}

