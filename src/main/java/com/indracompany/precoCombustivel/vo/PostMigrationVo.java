package com.indracompany.precoCombustivel.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Virtual object para fazer o payLoad dos dos dados após a finalização da migração.
 * @author dinarte
 */

@Data @AllArgsConstructor
public class PostMigrationVo {
	
	private String fileName;
 
	private String type;
	
	private Long size;

}
