package com.indracompany.precoCombustivel.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Entity @Data
public class User{
	
	@Id @GeneratedValue
	private Long id;
	
	@NotNull
	private String username;
	
	@NotNull
	private String password;

}
