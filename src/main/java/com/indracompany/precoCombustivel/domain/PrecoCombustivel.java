package com.indracompany.precoCombustivel.domain;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
 
@Entity @Data @AllArgsConstructor @NoArgsConstructor
@Table(indexes = {
		@Index(columnList = "municipio",unique=false, name="idx_municipio"),
		@Index(columnList = "regiao", unique=false, name="idx_regiao"),
		@Index(columnList = "bandeira", unique=false, name="idx_bandeira"),
		@Index(columnList = "regiao, estado, municipio, bandeira", unique=false, name="idx_regi_esta_muni_band_ordered")})
public class PrecoCombustivel {

	@Id @GeneratedValue
	private Long id;
	
	private String regiao;
	
	private String estado;
	
	private String  municipio;
	
	private String revenda;
	
	private String produto;
	
	private LocalDate dataColeta;
	
	private BigDecimal valorCompra;
	
	private BigDecimal valorVenda;
	
	private String unidadeMedida;
	
	private String bandeira;
	
}
