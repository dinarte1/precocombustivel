package com.indracompany.precoCombustivel.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(indexes = {@Index(columnList = "bandeira",unique=false, name="idx_preco_medio_bandeira")})
public class PrecoMedioPorBandeira {
	
	@Id @GeneratedValue
	private Long id;
	private String bandeira;
	private BigDecimal mediaCompra;
	private BigDecimal mediaVenda;

}
